class Complex {
  friend bool operator ==(const Complex& a, const Complex& b);
  friend bool operator +(const Complex& a, const Complex& b);
  double real;
  double imaginary;
 public:
  Complex( double r, double i = 0 );
};

bool operator ==( const Complex &a, const Complex &b );

bool operator +( const Complex &a, const Complex &b );
