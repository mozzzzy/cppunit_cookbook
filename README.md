# cppunit_cookbook
CppUnit Cookbook の内容をメモ.

## References
* CppUnit Cookbook
  <http://cppunit.sourceforge.net/doc/cvs/cppunit_cookbook.html>


## Note
### Sample Test Code
テストを書くために、テスト対象のソースコードを準備.  
複素数クラス. == は実部と虚部が共に等しい場合に true を返す.  

* src/ComplexNumber.hpp
  ```
  class Complex {
    friend bool operator ==(const Complex& a, const Complex& b);
    friend bool operator +(const Complex& a, const Complex& b);
    double real;
    double imaginary;
   public:
    Complex( double r, double i = 0 );
  };

  bool operator ==( const Complex &a, const Complex &b );

  bool operator +( const Complex &a, const Complex &b );
  ```

* src/ComplexNumber.cc
  ```
  #include "ComplexNumber.hpp"
  
  Complex::Complex( double r, double i ) : real(r), imaginary(i){}
  
  
  bool operator ==( const Complex &a, const Complex &b )
  {
    return a.real == b.real  &&  a.imaginary == b.imaginary;
  }
  
  
  bool operator +( const Complex &a, const Complex &b )
  {
    Complex *_complex = new Complex(a.real + b.real, a.imaginary + b.imaginary);
    return _complex;
  }
  ```


### Test Case
`==` が正常に動作することを確認する Test Case を作成する.  

* test/ComplexNumberTest.hpp  
  Test Case を作成するには、 TestCase クラスを使用する.  
  値をチェックしたい部分で、 CPPUNIT_ASSERT(bool) を呼び出す.  
  CPPUNIT_ASSERT(bool) の中では、true になるようにチェックしたい式を記述する.  

  ```
  class ComplexNumberTest : public CppUnit::TestCase {
   public:
    ComplexNumberTest( std::string name ) : CppUnit::TestCase( name ) {}
  
    void runTest() {
      CPPUNIT_ASSERT( Complex (10, 1) == Complex (10, 1) );
      CPPUNIT_ASSERT( !(Complex (1, 1) == Complex (2, 2)) );
    }
  };
  ```


### Fixture
複数の Test Case の中で共通の object を使いたい場合は、TestFixture クラスを使用する.  

* test/ComplexNumberTest.hpp を編集  
  それぞれのテストの中で利用する Object は、  
  CppUnit::TestFixture クラスを継承したクラスの中で、private メンバとして宣言する.  
  setUp() と tearDown() の override で、初期化と後処理を行う.  
  実行したいそれぞれのテストもこのクラスのメソッドとして定義しておく.  

  ```
  class ComplexNumberTest : public CppUnit::TestFixture {
  private:
    Complex *m_10_1, *m_1_1, *m_11_2;
  public:
    void setUp()
    {
      m_10_1 = new Complex( 10, 1 );
      m_1_1 = new Complex( 1, 1 );
      m_11_2 = new Complex( 11, 2 );  
    }
  
    void tearDown() 
    {
      delete m_10_1;
      delete m_1_1;
      delete m_11_2;
    }
  
    void testEquality()
    {
      CPPUNIT_ASSERT( *m_10_1 == *m_10_1 );
      CPPUNIT_ASSERT( !(*m_10_1 == *m_11_2) );
    }
  
    void testAddition()
    {
      CPPUNIT_ASSERT( *m_10_1 + *m_1_1 == *m_11_2 );
    }
  };
  ```


### TestCaller
CppUnit::TestFixture クラスで定義したテストを実行するために、  
CppUnit::TestCaller クラスのインスタンスを作成する.  

* test/Main.cc  
  CppUnit::TestCaller クラスのコンストラクタの第二引数は、  
  ComplexNumberTest のメソッドのアドレス.  
  test caller が実行されると、この指定されたメソッドが実行される.  
  以下の例だと、ComplexNumberTest::testEquality が実行される.  

  ```
  #include <cppunit/TestResult.h>
  #include "./ComplexNumberTest.hpp"
  
  int main() {
    // TestCaller クラスのインスタンスを生成  
    CppUnit::TestCaller<ComplexNumberTest> test( "testEquality", &ComplexNumberTest::testEquality );
    CppUnit::TestResult result;
    // TestCaller を実行
    test.run( &result );

    return 0;
  }
  ```


### TestSuite
複数のテストを一度に全て呼び出すためには、CppUnit::TestSuite クラスを使う.  

* test/Main.cc を編集  
  addTest メソッドで TestSuite のインスタンスにテストを追加する.  
  第一引数には、前述の TestCaller クラスのインスタンス.  

  ```
  #include <cppunit/TestSuite.h>
  #include <cppunit/TestResult.h>
  #include "./ComplexNumberTest.hpp"
  
  int main() {
  
    CppUnit::TestSuite suite;
    CppUnit::TestResult result;
  
    suite.addTest( new CppUnit::TestCaller<ComplexNumberTest>(
                           "testEquality",
                           &ComplexNumberTest::testEquality ) );
  
    suite.addTest( new CppUnit::TestCaller<ComplexNumberTest>(
                           "testAddition",
                           &ComplexNumberTest::testAddition ) );
    suite.run( &result );
  
    return 0;
  }
  ```


### TestRunner
test case を実行して、結果を取得、確認するには、TestRunner クラスを使う.  

* test/ComplexNumberTest.hpp に以下を追加  
  TestRunner が TestSuite にアクセスできるように、 
  上記の ComplexNumberTest クラスに、static メソッドで、以下を追加する.  

  ```
   public: 
    static CppUnit::Test *suite()
    {
      CppUnit::TestSuite *suiteOfTests = new CppUnit::TestSuite( "ComplexNumberTest" );
      suiteOfTests->addTest( new CppUnit::TestCaller<ComplexNumberTest>( 
                                     "testEquality", 
                                     &ComplexNumberTest::testEquality ) );
      suiteOfTests->addTest( new CppUnit::TestCaller<ComplexNumberTest>(
                                     "testAddition",
                                     &ComplexNumberTest::testAddition ) );
      return suiteOfTests;
    }
  ```

* test/Main.cc  
  更に、TestRunner.h のヘッダファイルの追加と、  
  CppUnit::TextUi::TestRunner の定義、  
  TestRunner への TestSuite の追加を行う.  

  ```
  #include <cppunit/ui/text/TestRunner.h>
  #include <cppunit/TestResult.h>
  #include "./ComplexNumberTest.hpp"

  int main( int argc, char **argv)
  {
    CppUnit::TextUi::TestRunner runner;
    runner.addTest( ComplexNumberTest::suite() );
    runner.run();
    return 0;
  }
  ```

  TestRunner は以下を出力する.  
  - test が全て成功した場合  
    - -> 成功した旨を知らせるメッセージ  
  - test が失敗した場合  
    - 失敗した test case  
    - 失敗したtest を含むソースファイル名  
    - 失敗した行番号  
    - 失敗した CPPUNIT_ASSERT()　を呼び出した行のtext の全て  


### Helper Macros
TestRunner が TestSuite にアクセスできるように static メソッドとして  
suite() メソッドを定義したが、これは繰り返しが多く、エラーを含めやすい.  
suite() メソッドは、マクロの Writing test fixture set を使えば自動的に生成できる.  

* test/ComplexNumberTest.hpp  
  Helper Macro を使用した場合の ComplexNumberTest は以下のコードになる.  
  まず、ヘッダファイルを追加.  

  ```
  #include <cppunit/extensions/HelperMacros.h>

  class ComplexNumberTest : public CppUnit::TestFixture  {
  ```

  次に ComplexNumberTest の定義の先頭で、suite を宣言する.  
  引数にはクラス名を渡す.  
  ```
  class ComplexNumberTest : public CppUnit::TestFixture {
    CPPUNIT_TEST_SUITE(ComplexNumberTest);
  ```
  
  次に それぞれの test case を宣言する.  
  ```
    CPPUNIT_TEST( testEquality );
    CPPUNIT_TEST( testAddition );
  ```
  
  最後に、suite の宣言を終了する.  
  ```
    CPPUNIT_TEST_SUITE_END();
  ```

  これで、`static CppUnit::TestSuite *suite();` の宣言が完了する.  
  これ以外の部分は変更しなくて良い.  


  test case から投げられる例外を比較したい場合、以下のように書くと良い.  
  ```
  CPPUNIT_TEST_SUITE( ComplexNumberTest );
  // ...

  // testDivideByZeroThrows という test case が MathException を投げる
  CPPUNIT_TEST_EXCEPTION( testDivideByZeroThrows, MathException );
  CPPUNIT_TEST_SUITE_END();
  ```

  修正後の test/ComplexNumberTest.hpp は以下.  

  ```
  #include <cppunit/extensions/HelperMacros.h>  // CPPUNIT_TEST_SUITE, CPPUNIT_TEST ...
  #include "../src/ComplexNumber.hpp"
  
  class ComplexNumberTest : public CppUnit::TestFixture {
   CPPUNIT_TEST_SUITE(ComplexNumberTest);
  
   CPPUNIT_TEST( testEquality );
   CPPUNIT_TEST( testAddition );
  
   CPPUNIT_TEST_SUITE_END();
  
   private:
    Complex *m_10_1, *m_1_1, *m_11_2;
   public:
    void setUp()
    {
      m_10_1 = new Complex( 10, 1 );
      m_1_1 = new Complex( 1, 1 );
      m_11_2 = new Complex( 11, 2 );
    }
  
    void tearDown()
    {
      delete m_10_1;
      delete m_1_1;
      delete m_11_2;
    }
  
    void testEquality()
    {
      CPPUNIT_ASSERT( *m_10_1 == *m_10_1 );
      CPPUNIT_ASSERT( !(*m_10_1 == *m_11_2) );
    }
  
    void testAddition()
    {
      CPPUNIT_ASSERT( *m_10_1 + *m_1_1 == *m_11_2 );
    }
  };
  ```


### TestFactoryRegistry
TestFactoryRegistry は二つの危険を回避するために作成された機能.  
  * fixture suite を test runner に追加し忘れる (別ファイルで定義する場合、容易に忘れる)  
  * test case の増加によって test code の編集に時間がかかる.  
TestFactoryRegistry は、初期化のタイミングで suite を登録する事ができる場所.  

ComplexNumberTest を TestFactoryRegistry に登録する場合、以下を記述する.  
* test/ComplexNumberTest.cc  
  ```
  #include <cppunit/extensions/HelperMacros.h>  // CPPUNIT_TEST_SUITE, CPPUNIT_TEST ...
  #include "./ComplexNumberTest.hpp"
  
  CPPUNIT_TEST_SUITE_REGISTRATION( ComplexNumberTest );
  ```

  こうする事で、Main.cpp 側では、suite を include する必要なく、以下のように記述できる.  

* test/Main.cc
  ```
  #include <cppunit/extensions/TestFactoryRegistry.h>
  #include <cppunit/ui/text/TestRunner.h>
  #include "./ComplexNumberTest.hpp"

  int main( int argc, char **argv)
  {
    CppUnit::TextUi::TestRunner runner;
    CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();
    runner.addTest( registry.makeTest() );
    runner.run();
    return 0;
  }
  ```


### Post-build check
  unit test を build process に組み込む場合、TestRunner クラスの run() メソッドの、  
  boolean 型の戻り値を利用する.  
  テストコードを以下のように修正.  
```
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include "./ComplexNumberTest.hpp"

int main( int argc, char **argv)
{
  CppUnit::TextUi::TestRunner runner;
  CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();
  runner.addTest( registry.makeTest() );
  bool wasSuccessful = runner.run();
  return !wasSuccessful;
}
```
