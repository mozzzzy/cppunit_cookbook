#include "ComplexNumber.hpp"

Complex::Complex( double r, double i ) : real(r), imaginary(i){}


bool operator ==( const Complex &a, const Complex &b )
{
  return a.real == b.real  &&  a.imaginary == b.imaginary;
}


bool operator +( const Complex &a, const Complex &b )
{
  Complex *_complex = new Complex(a.real + b.real, a.imaginary + b.imaginary);
  return _complex;
}
